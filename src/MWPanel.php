<?php
namespace MILEXA\WPAWESOME\ADDONS\MW;
use MILEXA\WPAWESOME\WC_MWViews;

if ( ! class_exists("MILEXA\\WPAWESOME\\ADDONS\\MW\\MWPanel") ) :
    class MWPanel
    {
        /**
         *
         */
        public static function init(){
            $class = __CLASS__;
            new $class;
        }

        /**
         * MWPanel constructor.
         */
        public function __construct(){}



        public static function integration(){
            $view = new WC_MWViews();
            $tpl = [
                "view"      => "mw.integrations",
                "addon"     => "mw-addon"
            ];
            $view->makeAddonView($tpl,[
                "extensions"    => self::getExtensionsList()
            ]);
        }

        private static function getExtensionsList(){
            try{
                $curl = curl_init("http://addons.amineabri.co.uk");
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $json_response = curl_exec($curl);
                curl_close($curl);
                $response = json_decode($json_response, true);
                return self::buildResult($response);
            }catch(\Exception $e){
                wp_die($e->getMessage());
            }
        }
        private static function buildResult($response){
            if(is_array($response) && $response['pagelen'] > 0){
                $data = [];
                foreach ($response['values'] as $v):
                    if (strpos($v['slug'], 'addon') !== false):
                        $parse = self::checkCurrentPlugin($v['slug']);
                        if(is_array($parse)):
                            $data[] = [
                                "name"              => $parse['name'],
                                "slug"              => $v['slug'],
                                "description"       => $v['description'],
                                "keywords"          => $parse['keywords'],
                                "website"           => $v['website'],
                                "status"            => $parse['status'],
                                "type"              => $parse['type'],
                                "version"           => $parse['version'],
                                "avatar"            => $v['links']['avatar']['href'],
                                "owner"             => [
                                    "display_name"      => $v['owner']['display_name'],
                                    "avatar"            => $v['links']['avatar']['href']
                                ],
                                "size"              => $v['size'],
                                "updated_on"        => $v['updated_on']
                            ];
                        endif;
                    endif;
                endforeach;
                return $data;
            }else{
                return [];
            }
        }
        private static function checkCurrentPlugin($plugin_name){
            $dirs = glob(AA_PATH.'vendor/ardevlabs/*-addon', GLOB_MARK);
            foreach ($dirs as $dir) {
                if (is_dir($dir)) {
                    $path        = AA_PATH . 'vendor/ardevlabs/' . basename($dir) . "/src/";
                    $setting     = self::read($path."setting.json");
                    if($setting['autoload'][0]["active"] && basename($dir) === $plugin_name):
                        return [
                            "name"          => $setting['name'],
                            "description"   => $setting['description'],
                            "keywords"      => $setting['keywords'],
                            "type"          => $setting['autoload'][0]['type'],
                            "version"       => $setting['version'],
                            "status"        => $setting['autoload'][0]["active"]
                        ];
                    endif;
                }
            }
            return false;
        }
        protected static function read($string){
            $autoload = self::convert(file_get_contents($string));
            $autoload = $autoload["system"];
            return $autoload;
        }
        protected static function convert($string){
            $data = json_decode($string,true);
            return $data;
        }

    }
endif;
