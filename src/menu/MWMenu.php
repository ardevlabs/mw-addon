<?php
namespace MILEXA\WPAWESOME\ADDONS\MW;
if ( ! class_exists("MILEXA\\WPAWESOME\\ADDONS\\MW\\MWMenu") ) :
class MWMenu
{
    public static function load_menu(){
      add_submenu_page(
          'edit.php?post_type=app_manager',
          'Integrations',
          'Integrations',
          'administrator',
          'mw-integration',
          ["MILEXA\\WPAWESOME\\ADDONS\\MW\\MWPanel", 'integration']
      );
    }

    /**
     * Remove the first menu from the backend sidebar
     */
    public static function removeFirstMenu() {
        global $submenu;

    }
}
endif;
