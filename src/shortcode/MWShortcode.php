<?php
namespace MILEXA\WPAWESOME\ADDONS\MW;

if ( ! class_exists("MILEXA\\WPAWESOME\\ADDONS\\MW\\MWShortcode") ) :
class MWShortcode
{
    public static function init()
    {
        $class = __CLASS__;
        new $class;
    }

    public function __construct(){

    }

}
endif;