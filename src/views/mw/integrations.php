<?php
global $SF_DATA_VIEW;
function getSize($bytes){
    $s = array('b', 'Kb', 'Mb', 'Gb');
    $e = floor(log($bytes)/log(1024));
    return sprintf('%.2f '.$s[$e], ($bytes/pow(1024, floor($e))));
}
?>
<div class="wrap plugin-install-tab-popular">
    <div class="wp-filter">
        <ul class="filter-links">
            <li class="plugin-install-featured"><a href="#" class="current" aria-current="page">Featured</a> </li>
            <li class="plugin-install-popular"><a href="#">Popular</a> </li>
            <li class="plugin-install-recommended"><a href="#">Recommended</a> </li>
            <li class="plugin-install-favorites"><a href="#">Favorites</a></li>
        </ul>
        <form class="search-form search-plugins" method="get">
            <input type="hidden" name="tab" value="search">
            <label class="screen-reader-text" for="typeselector">Search plugins by:</label>
            <label><span class="screen-reader-text">Search Plugins</span>
                <input type="search" name="s" value="" class="wp-filter-search" placeholder="Search extension..." aria-describedby="live-search-desc">
            </label>
            <input type="submit" id="search-submit" class="button hide-if-js" value="Search Plugins">	</form></div>
    <br class="clear">
    <form id="plugin-filter" method="post">
        <div class="wp-list-table widefat plugin-install">
            <h2 class="screen-reader-text">integrations list</h2>
            <div id="the-list">
                <?php
                if(is_array($SF_DATA_VIEW) && count($SF_DATA_VIEW) > 0):
                    foreach ($SF_DATA_VIEW['extensions'] as $value):
                        ?>
                        <div class="plugin-card plugin-card-<?php echo $value['slug']; ?>">
                            <div class="plugin-card-top">
                                <div class="name column-name">
                                    <h3>
                                        <a href="#" class="thickbox open-plugin-details-modal">
                                            <?php echo $value['name']; ?>
                                            <img src="<?php echo $value['avatar']; ?>" class="plugin-icon" alt="">
                                        </a>
                                    </h3>
                                </div>
                                <div class="action-links">
                                    <ul class="plugin-action-buttons">
                                        <li><a class="install-now button <?php if($value['status']):?>disabled<?php endif; ?>" href="#" "><?php if($value['status']):?>Active<?php else: echo "Install Now"; endif; ?></a></li>
                                    </ul>
                                </div>
                                <div class="desc column-description">
                                    <p><?php echo $value['description']; ?></p>
                                    <p class="authors">
                                        <cite>By <a href="#"><?php echo $value['owner']['display_name']; ?></a></cite>
                                    </p>
                                </div>
                            </div>
                            <div class="plugin-card-bottom">
                                <div class="column-updated">
                                    <strong>Last Updated:</strong> <?php echo human_time_diff( strtotime( $value['updated_on']), current_time('timestamp') ) . ' ago'; ?>
                                </div>
                                <div class="column-compatibility">
                            <span class="compatibility-compatible">
                                <strong>Size: </strong> <?php echo getSize($value['size']); ?>
                            </span>
                                </div>
                            </div>
                        </div>
                        <?php
                    endforeach;
                endif;
                ?>
            </div>
        </div>
    </form>

</div>
